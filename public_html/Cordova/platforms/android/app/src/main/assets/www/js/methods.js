methods = {
    //Inserta en el HTML 
    template: function(label, data){
        var template = $$(label).html();
        var compiledTemplate = Template7.compile(template);
        var html = compiledTemplate(data);
        $$(label).html(html);
        $$(label).show();
            
        // console.log("t", data);
        
        delete label;
        delete data;
        delete html;


      },
    mapa: function() {
      
      directionsService = new google.maps.DirectionsService;
      directionsDisplay = new google.maps.DirectionsRenderer;
 
      var MarkerBus = {lat: 41.106212, lng: 1.245496};//Inicializacion del punto del bus en el mapa
      var MiUbi = new google.maps.LatLng(41.106212, 1.245496);//Inicializacion de la variable que sera nuestra ubicacion
      var busUbication = new google.maps.LatLng(latitudBus, longitudBus);//Inicializacion de la variable que sera la ubicacion del autobus
        
      var centrado=true;//Variable que comprovara si estamos centrados
      ConductorAutobus = false;//Variable que determina si somos conductor o cliente        
        
      crearIcons= true;//Variable para saver si tenemos que crear los iconos
      CrearWaypoints=true;//Variable para saber si tenemos que crear los puntos
      CrearDirections=true;//Variable para saber si tenemos que crear las direcciones
        
      waypts=[];//Vaciamos la array para volver a meter puntos del mapa 
       
      //Creacion del mapa
      map = new google.maps.Map(document.getElementById('map'), {

        center: MiUbi,
        zoom: 18,
        disableDefaultUI: true 
      
      });

      directionsDisplay.setMap(map);


      if (navigator.geolocation.getCurrentPosition) {
          
      navigator.geolocation.watchPosition(function(position) {
                      
        MiUbi = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);//Variable de nuestra ubicacion
        busUbication = new google.maps.LatLng(latitudBus, longitudBus);//Variable de la ubicacion del autobus

        //Variable que recoge los parametros de la ubicacion y los introduce en una array 
        posGps = [position.coords.latitude,position.coords.longitude];
        miUbiLat=posGps[0];
        miUbiLng= posGps[1];

        app.methods.getParadasPorRuta(nombreLiniaVisualizada);
        
        if (start.length < 1 || start == undefined) {

          console.log("Esperando los puntos...");

        }else{
          
          
          
          if (CrearDirections==true) {

            var primeraparada = new google.maps.LatLng(start[0], start[1]);
            var ultimparada =new google.maps.LatLng(end[0], end[1]);

            var request = {
              origin: primeraparada,
              destination: ultimparada,
              travelMode: 'DRIVING',
              waypoints:waypts,
            };

            directionsService.route(request, function(result, status) {
              if (status == 'OK') {
                
                directionsDisplay.setDirections(result);

              }else{

                console.log(status);
              
              }
            });
 
            CrearDirections=false;   
          }
        }


        var LatitudSave= miUbiLat;

        if (centrado){

          map.setCenter(MiUbi);
          centrado = false;

          }else{
              
            $(".loading-bro").hide();

          }  
            
          if (!ConductorAutobus) {

            marker_gps.setPosition(MiUbi);//ubicacion cliente
            marker_gps.setIcon(iconGPS);//icono cliente
            marker_bus.setPosition(busUbication);//ubicacion bus
            marker_bus.setIcon(iconBUS);//icono bus

          }else{

            marker_gps.setPosition(MiUbi);//ubicacion bus-cliente
            marker_gps.setIcon(iconGPS);//icono bus

          }

      });

      }

      if (!ConductorAutobus) {

        var iconGPS = { url: 'img/geo3.png',scaledSize: new google.maps.Size(43,37)};
        var iconGPSTransparente = { url: 'img/iconGPSTransparente.png',scaledSize: new google.maps.Size(43,37)};
        var marker_gps = new google.maps.Marker({position: MiUbi, map: map, icon: iconGPSTransparente});

        var iconBUS = { url: 'img/bus.png',scaledSize: new google.maps.Size(43,37)};
        var iconGPSTransparente = { url: 'img/iconGPSTransparente.png',scaledSize: new google.maps.Size(43,37)};
        var marker_bus = new google.maps.Marker({position: busUbication, map: map, icon: iconGPSTransparente});

      }else{

        var iconGPS = { url: 'img/bus.png',scaledSize: new google.maps.Size(43,37)}; 
        var iconGPSTransparente = { url: 'img/iconGPSTransparente.png',scaledSize: new google.maps.Size(43,37)};
        var marker_gps = new google.maps.Marker({position: MiUbi, map: map, icon: iconGPSTransparente});

      } 
    },

   

    setMapMyUbi: function (){
      //Funcion del boton que centra la ubicacion
      if (navigator.geolocation) {  

        navigator.geolocation.getCurrentPosition(function(position) { 

          MiUbi = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          map.setCenter(MiUbi);

        }); 

      }
    },

    Lector: function() {
            
      cordova.plugins.barcodeScanner.scan(function(result) {

        if (!result.cancelled) {

          document.getElementById("scannerText").value = result.text; 

        }

        }, function(error) {

          alert("El scanner ha fallado, reintentalo");

        });
    },

    BusMove: function (orden, paradas){
      
      var size=10; 
      var parada=orden-1;
      var movimiento= size*parada;
      var busTop  = parada*size;
      var largada = 10 * paradas;
      var duracion = orden/2;
             
      setTimeout(function () {

        $(".route .path").css("height", largada+"%"); //linea naranja

        $(":root").css("--PortcentajeDeMovimiento", movimiento+"%");
                    
        $(".line").css("height", (busTop+2)+"%");//linia verde
        $(".line").css("animation-duration", duracion+"s");

        $(".bus i").css("top", busTop+"%");//autobus              
        $(".bus i").css("animation-duration", duracion+"s");
       
      }, 200);

    },

    ClientMove: function (orden, paradas){
 
      var size=10; 
      var parada=orden-1;
      var movimiento= size*parada;
      var busTop  = parada*size;
      var largada = 10 * paradas;
      var duracion = orden/2;

      setTimeout(function () {

        $(":root").css("--PortcentajeDeMovimientoPersona", movimiento+"%");
          
        $(".linepersona").css("height", (busTop+2)+"%");//linia azul
        $(".linepersona").css("animation-duration", duracion+"s");

        $(".persona i").css("top",  busTop+"%");//persona
        $(".persona i").css("animation-duration", duracion+"s");
          
      }, 200); 

    },

    getRutaByCode: function(n){ 

      var codigo = n;

      app.request.post(api+'getRutaByCode', { code: codigo }, function (data) {

        var data = JSON.parse(data);
    
        var sender = {};

        sender.lista = data;

        app.methods.template('.listResult',sender);

        //Datos de la empresa
        $('#nombreEmpresa').html(data.empresa[0].Nombre_empresa);
        $('#telfEmpresa').html(data.empresa[0].Telefono_empresa);
        $('#correoEmpresa').html(data.empresa[0].Email_empresa); 

      });
    },

    getInfoParada: function(n){ 

      var nombre = n;
      app.request.post(api+'getInfoParada', { name: nombre },  function (data1) {

        var data1 = JSON.parse(data1);
        var sender = {};
        sender.lista1 = data1;
        app.methods.template('.listResult1',sender);

        //Actualiza POP-UP al hacer otra peticion
        $('#stop_name').html(data1[0].nombre);
        $('#route_name').html(data1[0].rutas_name);
        $('#stop_order').html(data1[0].orden);
        $('#stop_lat').html(data1[0].latitud);
        $('#stop_lng').html(data1[0].longitud);
        $('#stop_day').html(data1[0].dias.join('</br>'));
        $('#stop_hour').html(data1[0].horario.join('</br>'));

      });

    },

    getParadasPorRuta: function(n){ 
      
      if(posGps==null){
      
      }else if (posGps) {

        var nombre = n;
        app.methods.getInfoBus(n);
        app.request.post(api+'getParadasPorRuta', { name: nombre, pos: JSON.stringify(posGps) },  function (data2) {
          var data2 = JSON.parse(data2);//todas las paradas  
          var sender = {};//objeto que se enviara despues

          sender.lista2 = data2.points;//todos las paradas

          sender.next_stops = new Array();//array la parada

          //Arrays de Nombre, Longitud i latitud de cada punto
          MarkerPosLat = new Array();
          MarkerPosLng = new Array();
          MarkerNombre = new Array();
          for (var i = 0; i< data2.points.length; i++) {
            MarkerPosLat.push(data2.points[i].latitud);
            MarkerPosLng.push(data2.points[i].longitud);
            MarkerNombre.push(data2.points[i].nombre);
          }

          var PuntosDeRuta= new Array(); 
          for (var i = 0; i< MarkerPosLat.length; i++) {  
            
            if(i == MarkerPosLat.length){
              PuntosDeRuta.push({lat: parseFloat(MarkerPosLat[i]), lng: parseFloat(MarkerPosLng[i])});
            }else{
              PuntosDeRuta.push({lat: parseFloat(MarkerPosLat[i]), lng: parseFloat(MarkerPosLng[i])},);
            }
          } 
        
          sender.next_stops = [];
          LongitudPuntos=data2.points.length;
          LongitudPuntos=LongitudPuntos-1; 


          start = [];
          end=[];
          start[0]=data2.points[0].latitud;
          start[1]=data2.points[0].longitud;
          end[0]=data2.points[LongitudPuntos].latitud;
          end[1]=data2.points[LongitudPuntos].longitud;
        
          if (CrearWaypoints) {
            
            for (var i = 0; i<data2.points.length-2; i++) {
              var contadorPuntos=i+1;

              waypts.push({
                location: new google.maps.LatLng(data2.points[contadorPuntos].latitud,data2.points[contadorPuntos].longitud ),
                stopover:true
              });

            }
        
            CrearWaypoints=false;

          }


          var distancia=10000;
          for (var i = 0; i<data2.points.length; i++) {
            sender.next_stops.push(data2.points[i]);//entran todas
            metros = app.methods.getMetros(PuntosDeRuta[i].lat,PuntosDeRuta[i].lng,parseFloat(miUbiLat),parseFloat(miUbiLng));
            metros=parseFloat(metros);
            
            if (metros<distancia) {
              
             distancia=metros;
              distancia=parseFloat(distancia);
              paradacercana=data2.points[i];
            }
          }
          
          if (ConductorAutobus) {
          
            app.methods.BusMove(paradacercana.orden,LongitudPuntos);//llamada a guardar
            app.methods.postPosition(paradacercana.rutas_name,paradacercana.nombre,paradacercana.orden,JSON.stringify(posGps)); 

          }else if(!ConductorAutobus){
          
            app.methods.ClientMove(paradacercana.orden,LongitudPuntos);
            app.methods.getPosition(paradacercana.rutas_name,LongitudPuntos);
          }

          app.methods.template('.listResult2',sender);

        });    
      }
    },

    postPosition: function(ruta,parada,orden,position){
       
      var ruta= ruta;
      var parada= parada;
      var orden= orden;
      var latitud= posGps[0];
      var longitud= posGps[1];
    
      firebase.database().ref('seguimiento/'+ruta ).set({
        ruta: ruta,
        parada: parada,
        orden : orden,
        latitud :latitud,
        longitud:longitud
      },function(error) {
        if (error) {
        console.log("The write failed.");
        } else {
        console.log("Data saved successfully!")
        }
      });
      

    },

    getPosition: function(ruta,paradas){ 
      var informacion;
      var ruta    = ruta;
      var paradas = paradas;

      var seguimiento = firebase.database().ref('seguimiento/'+ruta);
      seguimiento.on('value', function(snapshot) {
      
      informacion = snapshot.val();
      var orden   = parseInt(informacion['orden']);
      latitudBus  = parseFloat(informacion['latitud']);
      longitudBus = parseFloat(informacion['longitud']);

      var size=10; 
      var parada=orden-1;
      var movimiento= size*parada;
      var busTop  = parada*size;
      var largada = 10 * paradas;
      var duracion = orden/2+2;
      
      if (!movimientoAntiguoBool) {
        movimientoAntiguo=movimiento;
      }else{
        movimientoAntiguo=0;
      }
             
      setTimeout(function () {
        $(".route .path").css("height", largada+"%"); //linea naranja
        if (movimientoAntiguo==0) {
        $(":root").css("--PortcentajeDeMovimientoAntiguo", movimientoAntiguo+"%");
        movimientoAntiguoBool=false;
        }else{
        $(":root").css("--PortcentajeDeMovimientoAntiguo", movimientoAntiguo+"%");
        // console.log(movimientoAntiguo);
        }
        $(":root").css("--PortcentajeDeMovimiento", movimiento+"%");
        
        

        $(".line").css("height", (busTop+2)+"%");//linia verde
        // $(".line").css("animation-name", "ball-roll"); //linea verde
        $(".line").css("animation-duration", duracion+"s");//autobus


        $(".bus i").css("top", busTop+"%");//autobus              
        // $(".bus i").css("animation-name", "ball-roll");//autobus 
        $(".bus i").css("animation-duration", duracion+"s");//autobus 
   
      }, 200); 
      
      });          

    },

    getParadasPorRutaData: function(n, onData){ 

      var nombre = n;
    
      app.request.post(api+'getParadasPorRuta', { name: nombre, pos: JSON.stringify(posGps) },  function (data2) {

        var data2 = JSON.parse(data2);

        if (onData) {
  
          var orden = data2.near_code.orden;
          var paradas = data2.points;
          onData(orden, paradas);

        }

      });

    },

    getMetros: function(lat1,lon1,lat2,lon2){

      rad = function(x) {
        return x*Math.PI/180;
      }

      var R = 6378.137; 
      var dLat = rad( lat2 - lat1 );
      var dLong = rad( lon2 - lon1 );
      var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      var d = (R * c)*1000;
      return d.toFixed(2);

    },

    getAllRoutes: function(){ 

      app.request.post(api+'getAllRoutes',  function (data3) {

        var data = JSON.parse(data3); 

        for (var i = 0; i<data.length; i++) {

          console.log("Name:",data[i].name,"Code:",data[i].code,"Code group:",data[i].code_groups);
          ListCode.push(data[i].code);
          ListCode.push(data[i].code_groups);

        }
      });
    },

    getInfoBus: function(ruta){

      app.request.post(api+'getInfoBus',{ruta: ruta},function (bus){

        var bus = JSON.parse(bus);
        var sender = {};
        sender.lista3 = bus;
        app.methods.template('.listResult3',sender);

        //Actualiza POP-UP al hacer otra peticion
        $('#bus_matricula').html(bus.matricula);
        $('#bus_descripcion').html(bus.descripcion);
        $('#bus_marca').html(bus.marca);
        $('#bus_modelo').html(bus.modelo);
        
      });
      
    },  

    setVariables: function() {
      CrearDirections=true;
      CrearWaypoints=true;
      directionsDisplay.setMap(null); 
    }


  }

// Carga todo
    document.addEventListener("DOMContentLoaded", function(event) {

    })

 