routes = [
  {
    path: '/',
    url: './index.html',
  },
   {
    path: '/rutas/',
    url: './pages/rutas.html',
  },
  {
    path: '/mapa/:name',
    url: './pages/mapa.html',
  },
  {
    path: '/lista/:name',
    url: './pages/lista.html',
  },
  {
    path: '/objetos/',
    url: './pages/objetosperdidos.html',
  }
];
