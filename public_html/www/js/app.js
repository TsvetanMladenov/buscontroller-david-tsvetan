// Dom7
var $$ = Dom7;
var map;
var searchArea;
var api = "http://buscontrol.hellomovertis.com/api/";
var posGps; 
var username;
var ListCode = [];
var MarkerPosLat = new Array();
var MarkerPosLng = new Array();
var MarkerNombre = new Array();
var PuntosDeRuta = new Array();
var start = new Array();
var end = new Array();
var miUbiLat = 0;
var miUbiLng = 0;
var nombreLiniaVisualizada;
var centrado = true;
var markers = new Array();
var LongitudPuntos;
var ConductorAutobus = true ;
var Area = true;
var ParadaCercana;
var crearIcons=true;
var latitudBus;
var longitudBus;
var ExisteBU=false;
var directionsService;
var directionsDisplay;
var waypts = new Array();
var CrearDirections=true;
var CrearWaypoints=true;
var movimientoAntiguoBool=true;
var movimientoAntiguo;
var movimiento;
var color;



// Framework7 App main instance
var app  = new Framework7({
  root: '#app', // App root element
  id: 'com.movertis.buscontrol', // App bundle ID
  name: 'Buscontrol', // App name
  theme: 'auto', // Automatic theme detection
  on: {
    // EVENTOS una vez cargada esta pagina se ejecuta
    pageInit: function (page) {
       
       if(page.name=="mapa"){ //si no le quito las 2 /mapa/ no funciona el mapa no se llega a cargar
         app.methods.mapa();

       } else if(page.name=="rutas"){
          app.methods.getRutaByCode(username);

       } else if(page.name=="objetosperdidos"){
          app.methods.getObjetos();
       }

    },
     // EVENTOS una vez cargada esta pagina se ejecuta el POPup
    popupOpen: function (popup) {
      // do something on popup open
    },
  },
  // App root methods
  methods: methods,
  // App routes
  routes: routes,
});

// Init/Create main view
var mainView = app.views.create('.view-main', {
  url: '/'
});


// ------------ Popup Screen --------------------
$$('#popup-Screen .login-button').on('click', function () {

  // Close login screen
  app.loginScreen.close('#popup-Screen');
    
});

$$('#popup-BusInfo .login-button').on('click', function () {

  // Close login screen
  app.loginScreen.close('#popup-BusInfo');
    
});

$$('#popup-getImagen .login-button').on('click', function () {

  // Close login screen
  app.loginScreen.close('#popup-getImagen');
    
});




// ------------ Popup Login --------------------

    //Al iniciar se carga la pantalla de login
    app.loginScreen.open('#my-login-screen');

// Login Screen Demo
$$('#my-login-screen .login-button').on('click', function () {
   username = $$('#my-login-screen [name="username"]').val();
  //var password = $$('#my-login-screen [name="password"]').val();
  var n = typeof(username);
  console.log(n);

  // Close login screen
  app.loginScreen.close('#my-login-screen');


    //comprobacion del codigo si es correcto
  var encontrado = false;
  for (var i = ListCode.length - 1; i >= 0; i--) {
    
     if ( username == ListCode[i]){
       encontrado = true; 
      }
  }
  //si lo encuentra ejecuta lo siguiente:
  if ( encontrado){
    // app.dialog.alert('Codigo correcto');
  }
  else{
        // Alerta codigo invalido
        app.dialog.alert('Código inexistente, introduzca uno correcto');
        app.loginScreen.open('#my-login-screen');
      }
// ----------------------------------------------- 
 
});



