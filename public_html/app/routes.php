<?php 

	//////////////////////////////////////
	// RUTAS ->add(new App\Middlewares\IsOwner(array('Groups','code')))
	//////////////////////////////////////

//

$_SESSION['owner'] = "TEST";




$app->group("/intranet" ,function() use ($app){

	$app->any("/",   						'App\Controllers\HomeController:index');
	// $app->any("/",   						'App\Controllers\HomeController:job_map');
	$app->any("/filtarRutas",   			'App\Controllers\HomeController:filtarRutas');
	$app->post("/map_change_route",   		'App\Controllers\HomeController:select_routes_then_change');
	$app->post("/map_change_group",   		'App\Controllers\HomeController:select_groups_then_change');

	
		$app->group("/routes" ,function() use ($app){

			$app->get("/",    		 			'App\Controllers\RoutesController:routes');
			$app->post("/modify",  				'App\Controllers\RoutesController:mod_routes');
			$app->post("/add",		 			'App\Controllers\RoutesController:add_routes');
	    	$app->get("/delete/{code}", 		'App\Controllers\RoutesController:delete_routes');
	    	$app->get("/modifyroutes/{code}", 	'App\Controllers\RoutesController:modifyroutes');
			
		});

		$app->group("/group" ,function() use ($app){

			$app->get("/",  				'App\Controllers\GroupController:groups');
			$app->post("/add", 				'App\Controllers\GroupController:add_group');
			$app->post("/mod", 				'App\Controllers\GroupController:mod_group') ;
			$app->get("/delete/{code}",		'App\Controllers\GroupController:delGroup');
			$app->get("/list",  			'App\Controllers\GroupController:list_group');			
		});
 
		$app->group("/points" ,function() use ($app){

			$app->get("/",  				'App\Controllers\PointController:points');
			$app->post("/add", 				'App\Controllers\PointController:add_point');
			$app->post("/mod", 				'App\Controllers\PointController:mod_point') ;
			$app->get("/delete/{code}",		'App\Controllers\PointController:del_point');
			$app->get("/list",  			'App\Controllers\PointController:list_group');			
		});

		$app->group("/import" ,function() use ($app){

			$app->get("/", 	 				'App\Controllers\ImportController:import_routes');
			$app->post("/add", 	 			'App\Controllers\ImportController:add_routes');
			$app->post("/addparada", 	 	'App\Controllers\ImportController:add_parada');
			$app->post("/addpersonas", 	 	'App\Controllers\ImportController:add_personas');
			
		});

		$app->group("/bus" ,function() use ($app){

			$app->get("/", 	 					'App\Controllers\BusController:bus');
			$app->post("/add", 	 				'App\Controllers\BusController:add_bus');
			$app->get("/delete/{code}", 		'App\Controllers\BusController:delete_bus');
			$app->post("/mod",  				'App\Controllers\BusController:mod_bus');
			
		});

		$app->group("/lista" ,function() use ($app){

			$app->get("/",    		 			'App\Controllers\ListaController:lista');
			$app->post("/mod_lista",  			'App\Controllers\ListaController:mod_lista');
			$app->post("/add_user",		 		'App\Controllers\ListaController:add_user');
	    	$app->get("/delete/{name}", 		'App\Controllers\ListaController:delete_user');
	    	$app->get("/modifylista/{name}",    'App\Controllers\ListaController:modifylista');
			
		});

		$app->group("/personas" ,function() use ($app){

			$app->get("/",    		 			'App\Controllers\PersonaController:personas');
			$app->post("/add_persona",  		'App\Controllers\PersonasController:add_persona');
			$app->get("/del_persona/{dni}",		'App\Controllers\PersonasController:del_persona');
	    	$app->post("/mod_persona", 			'App\Controllers\PersonasController:mod_persona');
	    	
			
		});

		$app->group("/objetosPerdidos" ,function() use ($app){

			$app->get("/",    		 			'App\Controllers\ObjetosPerdidosController:ObjetosPerdidos');
			$app->post("/modify",  				'App\Controllers\ObjetosPerdidosController:mod_objeto_perdido');
			$app->post("/add",					'App\Controllers\ObjetosPerdidosController:add_objeto_perdido');
	    	$app->get("/delete/{nombre}", 		'App\Controllers\ObjetosPerdidosController:delete_objeto_perdido');
			
		});
});

	/**
	 * 
 	*/


	/**
	 * 
 	*/


$app->group("/api" ,function() use ($app){
		/**
		 * 
		 */$app->post("/getAllRoutes",  			'App\Controllers\api\apiController:getAllRoutes');		   
		   $app->get("/getParadas",					'App\Controllers\api\apiController:getParadas');
		   $app->any("/getInfoParada",				'App\Controllers\api\apiController:getInfoParada');
		   $app->any("/getParadasPorRuta",			'App\Controllers\api\apiController:getParadasPorRuta');
		   $app->any("/getRutaByCode",				'App\Controllers\api\apiController:getRutaByCode');
		   $app->any("/getInfoBus",					'App\Controllers\api\apiController:getInfoBus');
		   $app->any("/postBusPosition",			'App\Controllers\api\apiController:postBusPosition');
		   $app->any("/getBusPosition",				'App\Controllers\api\apiController:getBusPosition');
		   $app->any("/getApiInfo",					'App\Controllers\api\apiController:getApiInfo');
		   $app->any("/getColor",					'App\Controllers\api\apiController:getColor');
		   $app->any("/getLista",					'App\Controllers\api\apiController:getLista');
		   $app->any("/getObjetos",					'App\Controllers\api\apiController:getObjetos');
		   $app->any("/getImagen",					'App\Controllers\api\apiController:getImagen');
		   



		$app->group("/pairing" ,function() use ($app){
		 
			$app->post("/send_code",  		 'App\Controllers\api\pairingController:send_code');
			$app->post("/verification_code", 'App\Controllers\api\pairingController:verification_code');
		});
		/**
		 * 
		 */


		
		
		$app->group("" ,function() use ($app){
			
			$app->any("/dashboard",  						'App\Controllers\api\apiController:dashboard');
			$app->any("/tasks",  							'App\Controllers\api\apiController:tasks');
			$app->any("/user",  							'App\Controllers\api\apiController:user');
			$app->any("/stations/{la}/{lo}[/{radius}]",		'App\Controllers\api\apiController:stations');
		
		});//->add(new App\Middlewares\AuthREST());
		
});
