<?php

namespace App\Controllers\cronjobs;


use App\Models\User;
use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongolo;
use App\Controllers\firebase\firebase_ImportController;



class fuel_CronjobsController extends Controller
{

	public function read($request, $response, $arg=[])
	{

 
		$agent = $_SERVER['HTTP_USER_AGENT'];
		$hash = hash('SHA512', $prod_id.$txnRef.$mackey);

		$context = stream_context_create(array(
					  'http' => array(
					    'method' => 'GET',
					    'agent'  => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0',
					    'header' => "Content-Type: type=application/json\r\n"
					        . "X-Api-Signature: $hash"
					    )
					  )
					);

		date_default_timezone_set('Europe/Madrid');

		$url_returns = file_get_contents("https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres/", false, $context);
		echo "<pre>";
		$url_returns = json_decode($url_returns, true);
		
 
		$out = $url_returns['ListaEESSPrecio'];
		
		$this->Mongoo->Swish("Gas_Stations");

		$this->Mongoo->Collection->drop();

		for($i=0;$i<count($out);$i++){
			
			$data = array_values($out[$i]);

			$int = array('localizacion'=> array( 'tipo'=> 'Gasolinera','coordenadas'=> [  $this->remaster($data[3]),  $this->remaster($data[5])]),
			      'horario'				=> $data[2],
			      'direccion'			=> $data[1],
				  'cp'					=>(int)$data[0],
				  'margen'				=>$data[6],
				  'biodiesel'			=>$this->remaster($data[8]),
				  'bioetanol'			=>$this->remaster($data[9]),
				  'gas_natural_compr'	=>$this->remaster($data[10]),
				  'gas_natural_liqua'	=>$this->remaster($data[11]),
				  'gases_liqua_petrol'	=>$this->remaster($data[12]),
				  'gasoilA'				=>$this->remaster($data[13]),
				  'gasoilB'				=>$this->remaster($data[14]),
				  'gasolina95'			=>$this->remaster($data[15]),
				  'gasolina98'			=>$this->remaster($data[16]),
				  'gasoilAextra'		=>$this->remaster($data[17]),
				  'provincia'			=>$data[18],
				  'remision'			=>$data[19],
				  'rotulo'				=>$data[20],
				  'tipoVenta'			=>$data[21],
				  'bioetanolproc'		=>$this->remaster($data[22]),
				  'stermetilicoporc'	=>$this->remaster($data[23]),
				  'idess'				=>(int)$data[24],
				  'idmunicipio'			=>(int)$data[25],
				  'idprovincia'			=>(int)$data[26],
				  'idccaa'				=>(int)$data[27],
				  'actualizado' 		=> date("d/m/Y H:i:s")
				);
	 			
	 		$this->Mongoo->Collection->insert($int);
			
			//$this->Fire->put('/Gas_Stations/'.$data[24].'/', $int);


		}
		 
	}

	function remaster($n){

		return (float)str_replace(",",".", $n);
	}
}
