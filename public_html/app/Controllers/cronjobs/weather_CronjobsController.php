<?php

namespace App\Controllers\cronjobs;


use App\Models\User;
use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongolo;
use App\Controllers\firebase\firebase_ImportController;



class weather_CronjobsController extends Controller
{

	private function info_tiempo_ciudad($city_name)
	{

		date_default_timezone_set('Europe/Madrid');

		$url="http://api.openweathermap.org/data/2.5/weather?q=".$city_name."&APPID=9a7510562bd1c1de35f527433df51d74";
		$response=file_get_contents($url);
		$content = json_decode($response, true);

		$reg					= array();
		
		$reg['localizacion']	= array( "tipo" => "tiempo", "coordenadas" => array((float)$content['coord']['lat'], (float)$content['coord']['lon']) );

		$reg['tiempo']			= array("principal" => $content['weather'][0]['main'], "descripción" => $content['weather'][0]['description']);

		$validar				= array_pop($reg['tiempo']);

		switch ($validar) {

	        case "clear sky"://cielo limpio
	        		$reemplazos     = array("descripcion"=> "icono.png");
	        break;

	        case "scattered clouds"://nubes dispersas
	        		$reemplazos     = array("descripcion"=> "icono1.png");
	        break;

	        case "broken clouds"://nubes rotas
	        		$reemplazos     = array("descripcion"=> "icono2.png");
	        break;  
	        
	        case "few clouds"://pocas nubes
	        		$reemplazos     = array("descripcion"=> "icono3.png");
	        break;  
	       
	        case "light rain"://lluvia ligera
			        $reemplazos     = array("descripcion"=> "icono4.png");
	        break;

	        case "overcast clouds"://nubes cubiertas
	    		    $reemplazos		= array("descripcion"=> "icono5.png");
	        break;

    	}

    	$reg['tiempo']          = array_replace($reg['tiempo'], $reemplazos);  
		$reg['temperatura']		= (float)$content['main']['temp'];
		$reg['pression']		= (float)$content['main']['pressure'];
		$reg['humedad']			= (float)$content['main']['humidity'];
		$reg['temp_min']		= (float)$content['main']['temp_min'];
		$reg['temp_max']		= (float)$content['main']['temp_max'];

		$reg['visibilidad']			= $content['visibility'];

		$reg['velocidad_viento']	= (int)$content['wind']['speed'];
		$reg['direccion_viento']	= (int)$content['wind']['deg'];

		$reg['ciudad']				= $content['name'];
		$reg['actualizado']			= date("d/m/Y H:i:s");

		echo ("<pre>");
		echo($validar);
		//print_r($reg);
		die();
		return $reg;
	
	}
		

	
	public function read($request, $response, $arg=[])
	{
		$this->Mongoo->Swish("weather");

		//$this->Mongoo->Collection->drop();



		//$ciudades 	= array("Madrid","Barcelona","Valencia","Sevilla","Zaragoza","Málaga","Murcia","Palma","Las Palmas de Gran Canaria","Bilbao","Alicante","Córdoba","Valladolid","Vitoria","La Coruña","Granada","Oviedo","Santa Cruz de Tenerife","Pamplona","Almería","San Sebastián","Burgos","Albacete","Santander","Castellón de la Plana","Logroño","Badajoz","Huelva", "Salamanca","Lleida","Tarragona","León","Cádiz","Jaén","Orense","Girona","Lugo","Santiago de Compostela","Cáceres","Melilla","Ceuta","Guadalajara","Toledo","Pontevedra","Palencia","Ciudad Real","Zamora","Mérida","Ávila","Cuenca","Huesca","Segovia","Soria","Teruel"); 
			
		$ciudades 	= array("Madrid"); 


		for ($i = 0; $i < count($ciudades); $i++) {
			$reg 	= $this->info_tiempo_ciudad($ciudades[$i]);
			//$this->Mongoo->Collection->insert($reg);
	 		//$this->Mongoo->Collection->update(array("ciudad" => $ciudades[$i]), $reg);
		}
	}

	function remaster($n){

		return (float)str_replace(",",".", $n);
	}
}
