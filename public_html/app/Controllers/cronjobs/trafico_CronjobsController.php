<?php

namespace App\Controllers\cronjobs;


use App\Models\User;
use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongolo;
use App\Controllers\firebase\firebase_ImportController;



class trafico_CronjobsController extends Controller
{


	public function update_info_traffic($bbox)
	{

		//$bbox			= "41.1211,1.2396;41.1098,1.2584";

		$url_trafico 	= "https://traffic.api.here.com/traffic/6.2/flow.json?app_id=DbuvZgTFCsOpef7iqSl4&app_code=jHbpfEF2G_3BFggA-C5o-w&bbox=".$bbox."&responseattributes=sh,fc";
		$response 		= file_get_contents($url_trafico);
		//echo $response;
		$content 		= json_decode($response, true);

		$out							= array();

		$elems							= $content['RWS'][0]['RW'];
		for ($i = 0; $i<count($elems); $i++) {
				$reg					= $elems[$i];
				$aux					= array();
				$aux['id_linea']		= $reg['LI'];
				$aux['descripcion']		= $reg['DE'];
				//$aux['flow_items']		= array();
				//for ($j = 0; $j<count($reg['FIS'][0]); $j++) {
					$fi					= $reg['FIS'][0]['FI'];
					//print_r($fi);
					$tmc 				= $fi[0]['TMC'];
					$aux['TMC']			= $tmc;
					$shp 				= $fi[0]['SHP'];//es un formato vectorial de almacenaminto
					$aux['SHP']			= $shp;
					$cf 				= $fi[0]['CF']; // Current Flow. Información del estado del flujo de la carretera
					$aux['CF']			= $cf;
				//}
				array_push($out, $aux);
		}
		$out['actualizado']				= date("d/m/Y H:i:s");
		$out['bbox']					= $bbox;
		return $out;

	}

	public function read($request, $response, $arg=[])
	{
		date_default_timezone_set('Europe/Madrid');

		$this->Mongoo->Swish("traffic");

		$boxes			= array("41.1211,1.2396;41.1098,1.258");

		for ($i = 0; count($boxes); $i++) {
			$reg		= get_info_traffic($boxes[$i]);
		}
		//$this->Mongoo->Collection->drop();

	 	$this->Mongoo->Collection->update(array("bbox" => $boxes[$i]), $reg);
	}


	/*function remaster($n){

		//return (float)str_replace(",",".", $n);
	}*/
}
