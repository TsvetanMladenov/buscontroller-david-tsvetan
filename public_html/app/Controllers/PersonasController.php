<?php 

namespace App\Controllers;


use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongoo;
 


class PersonasController extends Controller
{

	function personas($request, $response, $arg=[]){	
		//En esta función mostrará los autobuses que hay en el grupo BUS.
		$this->Mongoo->Swish("Personas");
		$out['personas'] = $this->Mongoo->read($this->Mongoo->Collection->find());

		// $this->Mongoo->Swish("Routes");
		// $out['routes'] = $this->Mongoo->read($this->Mongoo->Collection->find());

		return $this->view->render($response, 'lista/listamodify.twig',  $out);

	}

	function add_persona($request, $response, $arg=[]){
		//Entramos en el grupo BUS. Esta función nos ayudará principalmente a implementar nuevos autobuses.
		$this->Mongoo->Swish("Personas");
		$this->Mongoo->Collection->insert($_POST);
		return $response->withRedirect("/intranet/lista/");

	}
	function del_persona($request, $response, $arg=[]){
		$this->Mongoo->Swish("Personas"); 
		
		$this->Mongoo->Collection->remove(array("dni"=>$arg['dni'])); 
		
		return $response->withRedirect("/intranet/lista/");
	}

	function mod_persona($request, $response, $arg=[]){
		//Entramos en el grupo BUS, donde inicializaremos las variables que nos ayudarán a modificar los datos dentro de BUS.
		$this->Mongoo->Swish("Personas");

		$oldName=		$_POST['old_name'];	
		$newName=		$_POST['new_name'];	
		$oldDni=		$_POST['old_dni'];	
		$newDni=		$_POST['new_dni'];
	

		$this->Mongoo->Collection->update(array("dni" => $oldDni), 		 array('$set' => array("name" => $newName,"dni" => $newDni))); 
		



		return $response->withRedirect("/intranet/lista/");
	}


	
}
