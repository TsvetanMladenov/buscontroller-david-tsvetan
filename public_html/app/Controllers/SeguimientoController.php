<?php 

namespace App\Controllers;


use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongoo;


class GroupController extends Controller
{

	function setParada($request, $response, $arg=[]){
		//Inicializamos las variables en las cuales almacenaremos los datos del $_POST.
		$name 		= $_POST['nombre'];
		$route 		= $_POST['ruta'];
		$vehicle	= $_POST['vehiculo'];
		$position 	= $_POST['posicion'];

		$this->Mongoo->Swish("Seguimiento");
		$position = $this->Mongoo->Collection->findOne(array("ruta"=>$route, "vehiculo"=>$vehicle));

		if ($position == FALSE) {

			$this->Mongoo->Collection->insert(array("ruta"=>$route, "vehiculo"=>$vehicle), array('$set' => array("position" => $position)) );

		} else {

			$this->Mongoo->Collection->update(array("ruta"=>$route, "vehiculo"=>$vehicle), array('$set' => array("position" => $position)) );

		}
 
	}

	function getUltimaParada($request, $response, $arg=[]){
		//Entrará en Seguimiento.
		$this->Mongoo->Swish("Seguimiento");
		//Creamos la variable FOUND, donde si encuentra una posición, será cambiada a TRUE y nos enseñará esa última parada, si no, será false.
		$found = false;
		$position = $this->Mongoo->read($this->Mongoo->Collection->findOne(array("posicion"=>$position)) );

		if(count($position)>0){
		$found = true;

		echo $position;

		}

	}
}