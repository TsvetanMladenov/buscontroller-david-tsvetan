<?php 

namespace App\Controllers;


use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongoo;
 


class PointController extends Controller
{

	function points($request, $response, $arg=[]){
		$arrayPoint 		= array();
		$arrayRoute 		= array();

		$this->Mongoo->Swish("Points"); //Entramos en la tabla PUNTOS/PARADAS y lo buscamos en la base de datos.

		//se transforma el array para el modal de modificar
		$out['points']  = $this->Mongoo->read($this->Mongoo->Collection->find());

		for ($i = 0; $i<count($out['points']); $i++) {
			$out['points'][$i]['horario'] = implode(';', $out['points'][$i]['horario']);
		}
		
		for ($i = 0; $i<count($out['points']); $i++) {
			$out['points'][$i]['dias'] = implode(';', $out['points'][$i]['dias']);
		}
		//se busca para rellenar la pantalla principal
		$arrayPoint  = $this->Mongoo->read($this->Mongoo->Collection->find());

		$this->Mongoo->Swish("Routes"); //Entramos en la tabla de rutas, buscándolo en la base de datos.

		$arrayRoute = $this->Mongoo->read($this->Mongoo->Collection->find());
		sort($arrayRoute);
		$out['routes'] = $arrayRoute;

		return $this->view->render($response, 'point/points.twig',  $out);		

	}

		function add_point($request, $response, $arg=[]){
		
		$this->Mongoo->Swish("Points");	//Entramos en points/paradas
		
		$horarioRaw	=$_POST['horarioRaw'];
		$_POST['horario']= explode(';', $horarioRaw);

		$this->Mongoo->Collection->insert($_POST);
		
		return $response->withRedirect("/intranet/points/");
				
		}
		function del_point($request, $response, $arg=[]){
		$this->Mongoo->Swish("Points"); //Entramos en points/paradas
		
		$this->Mongoo->Collection->remove(array("nombre"=>$arg['code'])); //Borra el código dentro del nombre.
		
		
		return $response->withRedirect("/intranet/points/"); //Y vuelve a puntos/paradas.
	}
		function mod_point($request, $response, $arg=[]){
		$this->Mongoo->Swish("Points"); //Se entra a la ruta Points/paradas
		
		$oldname=$_POST['old_name']; //Mostrará el antiguo nombre
		$newname=$_POST['new_name']; //Te dará la opción de poner un nuevo nombre
		$oldlat=$_POST['old_lat'];	//Te mostrará la antigua latitud
		$newlat=$_POST['new_lat'];	//Te dará la opción de poner una nueva latitud
		$oldlng=$_POST['old_lng'];	//Te mostrará la antigua longitud
		$newlng=$_POST['new_lng'];	//Te dejará insertar una nueva longitud
		$oldhorario=$_POST['old_horario'];	
		$newhorario=$_POST['new_horario'];	
		$olddias=$_POST['old_dias'];	
		$newdias=$_POST['new_dias'];
		$oldorden=$_POST['old_orden'];	
		$neworden=$_POST['new_orden'];	
		//Explode hace referencia a la separación por ';' entre los elementos de la variable dentro de los explode
		$oldhorarioArray   	=explode(';', $oldhorario);
		$newhorarioArray	=explode(';', $newhorario);
		$olddiasArray		=explode(';', $olddias);
		$newdiasArray		=explode(';', $newdias);
		//Se actualizan los datos dentro de la base de datos con el siguiente código y UPDATE. Una vez hecho, se cambia.
		$this->Mongoo->Collection->update(array("nombre" => $oldname), array('$set' => array("nombre" => $newname,"latitud" => $newlat,"longitud" => $newlng,"horario" => $newhorarioArray,"dias" => $newdiasArray,"orden" => (int)$neworden)));

		$cambiada	=$this->Mongoo->read($this->Mongoo->Collection->find(array("nombre"=>$newname),array("_id"=>0)));


		/*return $this->view->render($response, 'group/group.twig');*/
		return $response->withRedirect("/intranet/points/");
	}

		
}

		
	

	


