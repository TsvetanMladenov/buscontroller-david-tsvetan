<?php 

namespace App\Controllers;


use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongoo;
 


class ImportController extends Controller
{
	function import_routes($request, $response, $arg=[]){
	
		return $this->view->render($response, 'import/import_routes.twig');
	}	

	function add_routes($request, $response, $arg=[]){
	$this->Mongoo->Swish("Routes");
    $code = $this->randomCode();
        //carpeta donde se va a subir el archivo       
        $uploaddir = '/home/admin/web/buscontrol.hellomovertis.com/public_html/app/uploads/';
        //directorio + nombre del fichero
        $uploadfile = $uploaddir . basename($_FILES["filename"]["name"]);
        //mueve el archivo al directorio dicho        
        move_uploaded_file($_FILES["filename"]["tmp_name"], $uploadfile); 
        //abre el archivo i lo asigna a fp el cual lo importa a la base de datos.    
        $fp = fopen($uploadfile, "r");

        while(!feof($fp)) 
        {
            $linea = fgets($fp); //abre el archivo, r le da permisos de lectura, mientras que w son permisos de escritura.
            $data = explode(",",$linea);
     
                $datos = array(
                    "name" =>  utf8_encode($data[0]), 
                    "groups_name" => utf8_encode($data[1]) ,
                    "code_groups" => (int)$data[2],
                    "code" => $code
                );
                               
                if(!empty($data[0]))
                $this->Mongoo->Swish("Routes");
                $this->Mongoo->Collection->insert($datos);
                      
        }            
        
        fclose($fp);                        
        return $response->withRedirect("/intranet/import/");
	
	}


    function add_parada($request, $response, $arg=[]){
    
        $this->Mongoo->Swish("Points");
        //carpeta donde se va a subir el archivo       
        $uploaddir = '/home/admin/web/buscontrol.hellomovertis.com/public_html/app/uploads/';
        //directorio + nombre del fichero
        $uploadfile = $uploaddir . basename($_FILES["filename"]["name"]);
        //mueve el archivo al directorio dicho        
        move_uploaded_file($_FILES["filename"]["tmp_name"], $uploadfile); 
        //abre el archivo i lo asigna a fp        
        $fp = fopen($uploadfile, "r");
        //sube el archivo a la base de datos.
        while(!feof($fp)) 
        {
            $linea  = fgets($fp); //abre el archivo, r le da permisos de lectura
            $data   = explode(",",$linea);

            $d      = array();
            $dias   = explode(';', $data[7]);
            for ($i = 0; $i<count($dias); $i++) {
                $dia = "";
                switch($i) {
                    case 0: $dia = "Lunes"; break;
                    case 1: $dia = "Martes"; break;
                    case 2: $dia = "Miércoles"; break;
                    case 3: $dia = "Jueves"; break;
                    case 4: $dia = "Viernes"; break;
                    case 5: $dia = "Sábado"; break;
                    case 6: $dia = "Domingo"; break;
                }
                if ($dias[$i] == "1") {
                    array_push($d, $dia);
                }
            $horario   = explode(';', $data[2]);

            }
                $datos = array(
                    "nombre" => utf8_encode( $data[0] ), 
                    "rutas_name" =>utf8_encode(  $data[1] ), 
                    "horario" => $horario, 
                    "latitud" => utf8_encode( $data[3] ),
                    "longitud" => utf8_encode( $data[4] ),
                    "observaciones" => utf8_encode( $data[5] ),
                    "dias" => $d,
                    "orden"=>(int)$data[6],
                    "diasRaw" => $data[7]

                );
                               
                if(!empty($data[0]))
                $this->Mongoo->Collection->insert($datos);
                      
        }            
        
        fclose($fp);                        
        return $response->withRedirect("/intranet/import/");
    }	

	function add_personas($request, $response, $arg=[]){
    
        $this->Mongoo->Swish("Personas");
        //carpeta donde se va a subir el archivo       
        $uploaddir  = '/home/admin/web/buscontrol.hellomovertis.com/public_html/app/uploads/';
        //directorio + nombre del fichero
        $uploadfile = $uploaddir . basename($_FILES["filename"]["name"]);
        //mueve el archivo al directorio dicho        
        move_uploaded_file($_FILES["filename"]["tmp_name"], $uploadfile); 
        //abre el archivo i lo asigna a fp el cual lo importa a la base de datos.    
        $fp = fopen($uploadfile, "r");

        while(!feof($fp)) 
        {
            $linea = fgets($fp); //abre el archivo, r le da permisos de lectura, mientras que w son permisos de escritura.
            $data = explode(",",$linea);
     
                $datos = array(
                    "name" =>  utf8_encode($data[0]), 
                    "dni" => utf8_encode($data[1]) ,
                    "lista_name" => utf8_encode($data[2])      
                );
                               
                if(!empty($data[1])){
                
                $this->Mongoo->Collection->insert($datos);
                    }
        }            
        
        fclose($fp);                        
        return $response->withRedirect("/intranet/import/");
    
    }

	
    function randomCode(){
            //Se generará un número aleatorio random entre 1000 y 9999.
        $code=rand(1000,9999);

        return $code;

    }
}