<?php 

namespace App\Controllers;


use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongoo;
 


class HomeController extends Controller
{
	function index($request, $response, $arg=[])
	{

		return $this->view->render($response, 'index.twig');
	}

	function job_map($request, $response, $arg=[]){
		

					if (isset($_POST['name'])) 
					{

						$rutaName 	= $_POST['name'];//asignas el post a variables '54';
						$pos 		= json_decode($_POST['pos']); //['41.116683', '1.248404'];
						$this->Mongoo->Swish("Points"); //entramos a la tabla puntos1
						//busca donde rutas_name sea igual que $rutaName sin mostrar ID
						$points 	= $this->Mongoo->read( $this->Mongoo->Collection->find(array("nombre"=>$rutaName) ) );

				 	 	return json_encode($points);
					}
					//listo nombre de rutas del imput select
					 $this->Mongoo->Swish("Routes");
					 $out['select_routes'] = $this->Mongoo->read($this->Mongoo->Collection->find());//encuentra todo lo de Rutas

					$this->Mongoo->Swish("Groups");//entramos a la tabla
					$out['groupsname'] = $this->Mongoo->read($this->Mongoo->Collection->find());//encuentra todo

					$this->Mongoo->Swish("Points");//entramos a la tabla
					$out['points'] = $this->Mongoo->read($this->Mongoo->Collection->find());//encuentra todo
					//Volvemos a la página anterior.
					return $this->view->render($response, 'job_map.twig', $out);
		
	}

	function select_routes_then_change($request, $response, $arg=[]){

		if (isset($_POST['name']))
		{

			if ($_POST['name']=="Todo") 
			{
					
					$this->Mongoo->Swish("Points");
					$points = $this->Mongoo->read( $this->Mongoo->Collection->find(array(),array("_id"=>0)) );

					echo json_encode($points);
			}else
			{

				$this->Mongoo->Swish("Points"); //Entramos en RUTAS, recorremos el array el cual alberga el nombe de la ruta y del grupo, así como las paradas. 

				$points = $this->Mongoo->read( $this->Mongoo->Collection->find(array('rutas_name'=>$_POST['name']),array("_id"=>0)) );
				echo json_encode($points);
			}		
					
		}

	}

	// function select_groups_then_change($request, $response, $arg=[]){
	// 	//Si existe la información de $_POST['name'] entará en rutas, buscará y leerá la información en ella.
	// 	if (isset($_POST['name'])) 
	// 	{
						
	// 		$this->Mongoo->Swish("Routes");
	// 		$out['show_routes'] = $this->Mongoo->read( $this->Mongoo->Collection->find(array("groups_name"=>(string)$_POST['name']), array("_id"=>0)) );

	// 		$out['points'] = array();
	// 		//buscamos todas las rutas que coincidan con el post(nombre) del <select> grupos.
	// 		for ($i = 0; $i < count($out['show_routes']); $i++) 
	// 		{ 
	// 				//Se entra en el grupo PUNTOS y se busca y lee la información de rutas.
	// 				$this->Mongoo->Swish("Points");
	// 				$points = $this->Mongoo->read( $this->Mongoo->Collection->find(array('rutas_name'=>$out['show_routes'][$i]['name']), array("_id"=>0)) );

	// 				$out['points'] = array_merge($out['points'], $points);
							
				
	// 		}

	// 		//Lo imprimimos.
	// 	  echo json_encode($out['points']);

	
	// 	}

	// }
	
}
