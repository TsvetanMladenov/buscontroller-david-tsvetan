<?php

namespace App\Controllers\api;


use App\Models\User;
use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongolo;
 

class apiController extends Controller
{
 	
	function getAllRoutes($request, $response, $arg=[]){

				//Entramos en rutas y todo lo que haya dentro de ella, lo agrupamos en $out['allRoutes'] y lo imprimimos.
				$this->Mongoo->Swish("Routes");  
				$allRoutes = $this->Mongoo->read($this->Mongoo->Collection->find(array(),array("_id"=>0)));
				$out['allRoutes'] = $allRoutes;
				echo json_encode($out['allRoutes']);
				
	}

	function getParadas($request,$response,$arg=[]){

				//Entramos en Puntos y todo lo que haya dentro, se albergará en $out['points'] e imprimiremos.
				$this->Mongoo->Swish("Points"); 
				$out['points'] = $this->Mongoo->read($this->Mongoo->Collection->find(array(),array("_id"=>0)));
				echo json_encode($out['points']);			
		
	}


	function getInfoParada($request,$response,$arg=[]){

				//En la variable paradaNombre guardaremos todos los nombres de las paradas de $_POST['name'], dada desde la API.
				$paradaNombre = $_POST['name'];
				$this->Mongoo->Swish("Points"); 
				//Una vez dentro de Puntos, buscaremos su nombre el cual es el que hay dentro de la base de datos, y lo imprimiremos.
				$points = $this->Mongoo->read($this->Mongoo->Collection->find(array("nombre"=>$paradaNombre),array("_id"=>0)));
				$out['points'] = $points;
				echo json_encode($out['points']);
		
	}


	function getParadasPorRuta($request,$response,$arg=[]){

				$rutaName 	= $_POST['name'];//'1';
				$pos 		= json_decode($_POST['pos']);//['41.106442', '1.245377']; 
				

				//vehiculo

				//Entramos en rutas y leemos, sólo, lo que haya en el nombre, que en este caso sería el número de la linea.
				$this->Mongoo->Swish("Routes");
				$vehiculo=$this->Mongoo->Collection->findOne(array("name"=>$rutaName),array('_id'=>0));
				//Guardamos la información de todas las matrículas en la variable $Matricula.
				$matricula=$vehiculo['matricula'];
				//Entramos en el grupo Bus, leyendo e imprimiendo lo que Matricula alberga dentro.
				$this->Mongoo->Swish("Bus");
				$bus=$this->Mongoo->read($this->Mongoo->Collection->find(array("matricula"=>$matricula),array("_id"=>0)));

				$out['vehiculo'] = $bus;



				//paradas


				//entramos en puntos
				$this->Mongoo->Swish("Points"); 
				//saca los puntos que concuerda con la variable ruta name

				$points = $this->Mongoo->read($this->Mongoo->Collection->find(array("rutas_name"=>$rutaName),array("_id"=>0)));
				

					// Ordenar las paradas por el campo "orden"
				usort($points, function($a, $b) use ($points) {
					if ((int)$a['orden'] == (int)$b['orden']) { return 0; }

					if ((int)$a['orden'] > (int)$b['orden']) { return 1; }
					if ((int)$a['orden'] < (int)$b['orden']) { return -1; }
					return 0;
				});

				$out['points'] = $points;
				if (empty($out['near_code'])) {
					$out['near_code'] = $points[0];
				}					
					echo json_encode($out);
					
	}


	function getRutaByCode($request,$response,$arg=[]){
		
				//Toda la información dentro de $_POST['code'] la guardaremos en la variable $code.
				$code 	= (int)$_POST['code'];
				//Se entra al grupo Routas y leemos tanto el código como el código grupo.
				$this->Mongoo->Swish("Routes");
				
				$ruta 	=$this->Mongoo->read($this->Mongoo->Collection->find(array("code"=>$code),array("_id"=>0)));
				$grupo	=$this->Mongoo->read($this->Mongoo->Collection->find(array("code_groups"=>$code),array("_id"=>0)));
				//Si el grupo está vacío, imprimiremos la ruta, así como entraremos en Empresas e imprimiremos la información de esta.
				if (empty($grupo)) {
					$out['ruta'] = $ruta;
					$codeempresa = $ruta[0]['code_empresa'];
					$this->Mongoo->Swish("Empresas");
					$out['empresa'] =$this->Mongoo->read($this->Mongoo->Collection->find(
						array("Code_empresa"=>$codeempresa),array("_id"=>0)));
				 	echo json_encode($out);
				}else{
					$out['ruta'] = $grupo;	//Si no está vacío, imprimiremos los grupos y la información de las empresas así como el código.
					$codeempresa = $grupo[0]['code_empresa'];
					$this->Mongoo->Swish("Empresas");
					$out['empresa'] =$this->Mongoo->read($this->Mongoo->Collection->find(
						array("Code_empresa"=>$codeempresa),array("_id"=>0)));					
				 	echo json_encode($out);
				}

	}	

	function getBusPosition($request,$response,$arg=[]){

		$ruta=$_POST['ruta'];

		$this->Mongoo->Swish("Seguimiento");

		$seguimiento = $vehiculo=$this->Mongoo->Collection->findOne(array("ruta"=>$ruta),array('_id'=>0));
		$out['orden']=$seguimiento['orden'];
		$out['latitud']=$seguimiento['latitud'];
		$out['longitud']=$seguimiento['longitud'];


		echo json_encode($out);

	}

	function postBusPosition($request,$response,$arg=[]){

		$ruta=$_POST['ruta'];
		$parada=$_POST['parada'];
		$orden=$_POST['orden'];
		$latitud=$_POST['latitud'];
		$longitud=$_POST['longitud'];
		$this->Mongoo->Swish("Seguimiento");

		$SeguimientoExistente=$this->Mongoo->read($this->Mongoo->Collection->find(array("ruta"=>$ruta),array("_id"=>0)));

		if (empty($SeguimientoExistente)) {
			$this->Mongoo->Collection->insert($_POST);

		 }else{
			$this->Mongoo->Collection->update(array("ruta" => $ruta), array('$set' => array("parada" => $parada,"orden" => $orden,"latitud"=>$latitud,"longitud"=>$longitud)));
		}

		$out['ruta'] = $ruta;
		$out['parada'] = $parada;
		$out['orden'] = $orden;
		$out['latitud'] = $latitud;
		$out['longitud'] = $longitud;
		echo json_encode($out);
		
	}

	function getInfoBus($request,$response,$arg=[]){

		$ruta=$_POST['ruta'];

		$this->Mongoo->Swish("Routes");

		$ruta= $this->Mongoo->Collection->findOne(array("name"=>$ruta),array('_id'=>0));
		$matricula= $ruta['matricula'];

		$this->Mongoo->Swish("Bus");

		$out['bus']= $this->Mongoo->Collection->findOne(array("matricula"=>$matricula),array('_id'=>0));

		echo json_encode($out);

	}

	function getColor($request,$response,$arg=[]){

		$name=$_POST['name'];

		$this->Mongoo->Swish("Routes");

		$ruta	= $this->Mongoo->Collection->findOne(array("name"=>$name),array('_id'=>0));
		$color 	= $ruta['color'];

		$out['color'] = $color; 

		echo json_encode($out);

	}

	function getLista($request,$response,$arg=[]){

		$nameRuta = $_POST['ruta'];

		$this->Mongoo->Swish("Lista");

		$lista = $this->Mongoo->Collection->findOne(array("route_name"=>$nameRuta),array('_id'=>0));
		$name_lista   =	$lista['name'];	 
		$out['lista'] = $lista;

		$this->Mongoo->Swish("Personas");

		$personas = $this->Mongoo->read($this->Mongoo->Collection->find(array("lista_name"=>$name_lista),array("_id"=>0)));
		$out['personas'] = $personas;	

		echo json_encode($out);

	}

	function getObjetos($request,$response,$arg=[]){

		$this->Mongoo->Swish("Objetos_Perdidos");

		$out['objetos'] = $this->Mongoo->read($this->Mongoo->Collection->find(array(),array("_id"=>0)));
		
		echo json_encode($out);

	}

	function getImagen($request,$response,$arg=[]){

		$name = 'ObjetoNoBorrar';//$_POST['name'];

		$this->Mongoo->Swish("Objetos_Perdidos");

		$objeto = $this->Mongoo->Collection->findOne(array("nombre"=>$name),array('_id'=>0));
		
		$out['imagen'] = $objeto['fotoURI'];

		echo json_encode($out);

	}

}