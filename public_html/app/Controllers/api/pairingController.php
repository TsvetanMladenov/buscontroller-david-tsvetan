<?php

namespace App\Controllers\api;


use App\Models\User;
use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongolo;
use Firebase\JWT\JWT;



class pairingController extends Controller
{
 	
 	private static $secret_key = 'AIzaSyCB5UP4nfRGuf**bQje1FE2o65n_vV82asQY@';
    private static $encrypt = ['HS256'];
    private static $aud = null;

 	/**
 	 * [Generate_Token description]
 	 * @param [type] $data [description]
 	 */
 	public static function Generate_Token($data)
    {
        $time = time();
        
        $token = array(
            'exp' => $time + 630720000000,
            'aud' => self::Aud(),
            'data' => $data
        );

        return JWT::encode($token, self::$secret_key);
    }

    /**
     * [Aud description]
     */
    private static function Aud()
    {
        $aud = '';
        
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $aud = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $aud = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $aud = $_SERVER['REMOTE_ADDR'];
        }
        
        $aud .= @$_SERVER['HTTP_USER_AGENT'];
        $aud .= gethostname();
        
        return sha1($aud);
    }

 	/**
 	 * [send_code description]
 	 * @param  [type] $n [description]
 	 * @return [type]    [description]
 	 */
	function send_code($n){

		$this->Mongoo->Swish("Users"); 

			$randomNum = substr(str_shuffle("0123456789"), 0, 4);

			$useresendex = "administracion@mzningenieria.com";
			$passesendex = "m0v3rt1s3s3nd3x2018";
			$remitente = "movertis";
			//Enviamos SMS con esendex
			$request_xml = "<?xml version='1.0' encoding='UTF-8'?>  
			 <messages>  
			  <accountreference>EX0105953</accountreference> 
			   <message>  
			     <from>".$remitente."</from>
				 <to>".$_POST['mobile']."</to>  
			     <type>SMS</type>  
			     <body>Su codigo de verificacion es: ".$randomNum."</body>  
			     <lang>es-ES</lang>  
			     <retries>1</retries>  
			   </message>  
			 </messages>";

		 
		$this->Mongoo->Collection->insert(array("")); 

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://api.esendex.com/v1.0/messagedispatcher');
			curl_setopt($ch, CURLOPT_USERPWD, $useresendex.':'.$passesendex);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 4);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $request_xml);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: close'));
			 
			//Execute the request and also time the transaction ( optional )
			$start = array_sum(explode(' ', microtime()));
			$result = curl_exec($ch);
			$xml = simplexml_load_string($result);
			$idesendex = $xml->messageheader["id"][0];
			$stop = array_sum(explode(' ', microtime()));
			$totalTime = $stop - $start;
		

		$usu = $this->Mongoo->Collection->findOne(array("Phone"=>(int)$_POST['mobile']));

		if (count($use)==0) {

			$this->Mongoo->Collection->insert(array("Phone"=>(int)$_POST['mobile'],"Code"=>$randomNum,"IdSendeX"=>$idesendex,"Uniqid"=>uniqid(),"Moment"=>time()));

		} else {

			$this->Mongoo->Collection->update(array("Phone"=>(int)$_POST['mobile']),array("Phone"=>(int)$_POST['mobile'],"Code"=>$randomNum,"IdSendeX"=>$idesendex,"Uniqid"=>uniqid(),"Moment"=>time()),array("upsert" => false));

		}

			curl_close($ch);

		echo json_encode($result);
	}

	/**
	 * [verification_code description]
	 * @return [type] [description]
	 */
	function verification_code(){

		$this->Mongoo->Swish("Users"); 

		$result = $this->Mongoo->Collection->findOne(array("Phone"=>(int)$_POST['mobile'],"Code"=>(string)$_POST['code']),array("_id"=>0));

		$out['status'] = 0;

		if(count($result)>0){

			$out['status'] = 1;
			$out['token']  = $this->Generate_Token($_POST['mobile']);
		}


		echo json_encode($out);

	}
}
