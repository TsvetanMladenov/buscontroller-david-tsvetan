<?php 

namespace App\Controllers;


use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongoo;
 


class RoutesController extends Controller
{

	function routes($request, $response, $arg=[]){
	
		$this->Mongoo->Swish("Routes");//entramos a la tabla de Rutas
		$out['routes'] = $this->Mongoo->read($this->Mongoo->Collection->find());//encuentra todo lo de Rutas

		$this->Mongoo->Swish("Bus");
		$out['bus'] = $this->Mongoo->read($this->Mongoo->Collection->find());

		$this->Mongoo->Swish("Groups");//entramos a la tabla de Rutas
		$out['show_groups'] = $this->Mongoo->read($this->Mongoo->Collection->find());//encuentra todo lo de Rutas

		for ($i = 0; $i < count($out['routes']); $i++) { //recorre el array de rutas
			if (array_key_exists('group_code', $out['routes'][$i])) { //Si la llave principal (código) de grupos existe en la raiz de rutas
				$group = $this->Mongoo->Collection->findOne(array('code'=>(int)$out['routes'][$i]['group_code']),array("_id"=>0));
				
				if ($group) { //Si se encuentra el grupo, se añadirá el nombre del grupo a las rutas para saber a qué grupo pertenece.
					$out['routes'][$i]['groups_name'] = $group['name'];
				} else {	  //Por el contrario, si el grupo no existe, estará vacío.
					$out['routes'][$i]['groups_name'] = 'vacío';
				}
			}
		}

		return $this->view->render($response, 'routes/routes.twig',  $out);

	}

	function mod_routes($request, $response, $arg=[]){
		
		$oldname=$_POST['old_name'];	
		$oldgroup=$_POST['old_groups_name'];

		$newname=$_POST['new_name'];
		$newgroup=$_POST['new_groups_name'];
		$newmatricula=$_POST['new_matricula'];
		$color=$_POST['color'];
		

		$this->Mongoo->Swish("Groups");
		
		$grupo=$this->Mongoo->Collection->findOne(array("name"=>$newgroup),array('_id'=>0));
		$code_group=$grupo['code'];

		
		$this->Mongoo->Swish("Routes"); 
																												
		$this->Mongoo->Collection->update(array("name" => $oldname), array('$set' => array("name" => $newname,"code_groups"=> $code_group,"matricula"=>$newmatricula,"color"=>$color,"groups_name" => $newgroup))); 

		// $this->Mongoo->Collection->update(array("groups_name" => $oldgroup), array('$set' => array("groups_name" => $newgroup)));


		$this->Mongoo->Swish("Points");

		$this->Mongoo->Collection->update(array("rutas_name" => $oldname), array('$set' => array("rutas_name" => $newname)),array("multiple" => true));


		$this->Mongoo->Swish("Lista");
	
		$this->Mongoo->Collection->update(array("route_name" => $oldname), array('$set' => array("route_name" => $newname)),array("multiple" => true));


		return $response->withRedirect("/intranet/routes/");
	}

	function add_routes($request, $response, $arg=[]){

		$this->Mongoo->Swish("Routes");	//Entramos a rutas
		$result_name = $this->Mongoo->read($this->Mongoo->Collection->find(array("name"=>$name)));	//Buscamos el nombre y si lo encuentra, lo leemos.
		$code = $this->randomCode(); //Si lo ha encontrado, le añade el $code el cual es random
		$this->Mongoo->Swish("Groups");
		$code_groups = $this->Mongoo->Collection->findOne(array("name"=>$_POST['groups_name']),array('_id'=>0));
	 
		$_POST['code_groups'] = $code_groups['code'];
		$_POST['code_empresa'] = $code_groups['code_empresa'];
		$this->Mongoo->Swish("Routes");
		$_POST['code'] = $code;
		$this->Mongoo->Collection->insert($_POST); //Se inserta en la raiz de $_POST
		
		return $response->withRedirect("/intranet/routes/");
			
	}

	function delete_routes($request, $response, $arg=[]){

		$this->Mongoo->Swish("Routes"); //Entramos a rutas
		$ruta = $this->Mongoo->Collection->findOne(array("code"=>(int)$arg['code'])); //Borramos la información seleccionada, reconocida por el CODE.
		$nameRuta = $ruta['name'];
		// echo "<pre>";
		// print_r ($ruta);
		// die;
		$this->Mongoo->Collection->remove(array("code"=>(int)$arg['code'])); //Borramos la información seleccionada, reconocida por el CODE.

		$this->Mongoo->Swish("Points");
		$this->Mongoo->Collection->remove(array("rutas_name"=>$nameRuta));

		$this->Mongoo->Swish("Lista");
		$Lista = $this->Mongoo->Collection->findOne(array("route_name"=>$nameRuta),array('_id'=>0));
		$nombreLista = $Lista['name'];
		$this->Mongoo->Collection->remove(array("route_name"=>$nameRuta));
		
		$this->Mongoo->Swish("Personas");
		$this->Mongoo->Collection->remove(array("lista_name"=>$nombreLista));


		return $response->withRedirect("/intranet/routes/");
	}

	function randomCode(){
	 		//Se generará un número aleatorio random entre 1000 y 9999.
		$code=rand(1000,9999);

		return $code;

	}

	
	function modifyroutes($request, $response, $arg=[]){
		$code=(int)$arg['code'];

		$this->Mongoo->Swish("Routes");
		$route = $this->Mongoo->Collection->findOne(array("code"=>$code),array('_id'=>0));
		$out['route']=$route;
		$ruta_name=$route['name'];
		$group_name=$route['groups_name'];
		$ruta_matricula=$route['matricula'];

		$this->Mongoo->Swish("Points");
		$points =$this->Mongoo->read($this->Mongoo->Collection->find(array("rutas_name"=>$ruta_name),array('_id'=>0)));
		$out['points']=$points;

		$this->Mongoo->Swish("Groups");
		$group =$this->Mongoo->read($this->Mongoo->Collection->find(array("name"=>$group_name),array('_id'=>0)));
		$groups = $this->Mongoo->read($this->Mongoo->Collection->find());
		$out['group']=$group;
		$out['groups']=$groups;

		$this->Mongoo->Swish("Bus");
		$bus = $this->Mongoo->read($this->Mongoo->Collection->find(array("matricula"=>$ruta_matricula),array('_id'=>0)));
		$buses = $this->Mongoo->read($this->Mongoo->Collection->find());
		$out['bus']=$bus;
		$out['buses']=$buses;
		
		return $this->view->render($response, 'routes/routesmodify.twig',  $out);

	}
}
