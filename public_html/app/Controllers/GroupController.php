<?php 

namespace App\Controllers;


use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongoo;
 


class GroupController extends Controller
{
	function groups($request, $response, $arg=[]){
		$this->Mongoo->Swish("Groups");//entramos a la tabla
		$out['groupsname'] = $this->Mongoo->read($this->Mongoo->Collection->find());//encuentra todo


		return $this->view->render($response, 'group/group.twig',$out );//devuelve todo
	}
	

	function mod_group($request, $response, $arg=[]){
		//Se entra en el grupo GROUPS, donde inicializaremos las variables que nos ayudarán a cambiar los datos dentro de este grupo.
		
		$oldname=$_POST['old_name'];
		$newname=$_POST['new_name'];

		$this->Mongoo->Swish("Groups");

		$this->Mongoo->Collection->update(array("name" => $oldname), array('$set' => array("name" => $newname)));

		$this->Mongoo->Swish("Routes");
		
		$this->Mongoo->Collection->update(array("groups_name" => $oldname), array('$set' => array("groups_name" => $newname)), array("multiple" => true));
		/*return $this->view->render($response, 'group/group.twig');*/
		return $response->withRedirect("/intranet/group/");
	}

	function add_group($request, $response, $arg=[]){
		//Creamos la variable FLAG y $code, el cual alberga la función randomCode
		$flag = true;
		$code = $this->randomCode();				
		//Entramos en GRUPOS
		$this->Mongoo->Swish("Groups");				
		 //Se crea la variable result_groups, en el cual está leyendo la tabla CODE, si CODE tiene/encuentra un numero generado por la función
		//randomCode, se suma 1.
		$result_groups = $this->Mongoo->read($this->Mongoo->Collection->find(array("Code"=>$code)));
			//Si el contador de result_groups es mayor a 0, se cierra $flag (false). Sería 0 si la función randomCode no diera un numero.
		if(count($result_groups)>0){
			$flag = false;
		}
			//En este paso sólo entraríamos si $flag siguiera en TRUE y comprobaría los mismos pasos que en GROUPS
		if($flag){

				$this->Mongoo->Swish("Routes");
				$result_rutes = $this->Mongoo->read($this->Mongoo->Collection->find(array("Code"=>$code)));

				if(count($result_rutes)>0){
					$flag = false;
				}
		}
			
		if($flag){

			$this->Mongoo->Swish("Groups");	
			//Flag está abierta, por lo que se insertan los datos $code en el array POST['code'].
			 $_POST['code'] = (int)$code;
			 $_POST['code_empresa'] = "1";
			 $this->Mongoo->Collection->insert($_POST);

			 return $response->withRedirect("/intranet/group/");
		}else{
			//Se vuelve a empezar el ciclo de la función ADD_GROUP si no se insertara ningún número.
			$this->add_group();
		}

		
	}


	function randomCode(){
	 	//Se generará un número aleatorio entre 1000 y 9999.
		$code=rand(1000,9999);

		return $code;

	}

	function delGroup($request, $response, $arg=[]){
		//En esta función se entrará a grupos y se eliminará el grupo seleccionado. Una vez borrado, se volverá a la página anterior.
		$this->Mongoo->Swish("Groups");
		$this->Mongoo->Collection->remove(array("code"=>(int)$arg['code']));
		
		$this->Mongoo->Swish("Routes");
		$ruta=$this->Mongoo->Collection->findOne(array("code_groups"=>(int)$arg['code']));
		$this->Mongoo->Collection->remove(array("code_groups"=>(int)$arg['code']));

		$this->Mongoo->Swish("Points");
		$nombreRuta=$ruta['name'];
		$this->Mongoo->Collection->remove(array("rutas_name"=>$nombreRuta));
		
		return $response->withRedirect("/intranet/group/");
		}


}
