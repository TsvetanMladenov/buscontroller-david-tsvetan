<?php 

namespace App\Controllers;


use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongoo;
 


class BusController extends Controller
{

	function bus($request, $response, $arg=[]){	
		//En esta función mostrará los autobuses que hay en el grupo BUS.
		$this->Mongoo->Swish("Bus");
		$out['bus'] = $this->Mongoo->read($this->Mongoo->Collection->find());

		//En esta función mostrará las rutas que hay en el grupo ROUTES.
		$this->Mongoo->Swish("Routes");
		$out['routes'] = $this->Mongoo->read($this->Mongoo->Collection->find());

		return $this->view->render($response, 'bus/bus.twig',  $out);

	}

	function add_bus($request, $response, $arg=[]){
		//Entramos en el grupo BUS. Esta función nos ayudará principalmente a implementar nuevos autobuses gracias al insert.
		$this->Mongoo->Swish("Bus");
		$this->Mongoo->Collection->insert($_POST);
		return $response->withRedirect("/intranet/bus/");

	}
	function delete_bus($request, $response, $arg=[]){
		//Se entra en el grupo BUS y se elimina lo seleccionado con remove.
		$this->Mongoo->Swish("Bus"); 
		
		$this->Mongoo->Collection->remove(array("matricula"=>$arg['code'])); 
		
		return $response->withRedirect("/intranet/bus/");
	}

	function mod_bus($request, $response, $arg=[]){
		//Entramos en el grupo BUS, donde inicializaremos las variables que nos ayudarán a modificar los datos dentro de BUS junto al bus.twig, una vez hecho hará un update.
		$this->Mongoo->Swish("Bus");

		$oldMat=	$_POST['old_matricula'];	
		$newMat=	$_POST['new_matricula'];
		$oldDesc=	$_POST['old_descripcion'];
		$newDesc=	$_POST['new_descripcion'];
		$oldMarca=	$_POST['old_marca'];
		$newMarca=	$_POST['new_marca'];
		$oldModelo=	$_POST['old_modelo'];
		$newModelo=	$_POST['new_modelo'];
		$oldCapacidad= $_POST['old_capacidad'];
		$newCapacidad= $_POST['new_capacidad'];

		$this->Mongoo->Collection->update(array("matricula" => $oldMat), 	array('$set' => array("matricula" => $newMat))); 
		$this->Mongoo->Collection->update(array("descripcion" => $oldDesc), array('$set' => array("descripcion" => $newDesc))); 
		$this->Mongoo->Collection->update(array("marca" => $oldMarca), 		array('$set' => array("marca" => $newMarca))); 
		$this->Mongoo->Collection->update(array("modelo" => $oldModelo), 	array('$set' => array("modelo" => $newModelo))); 
		$this->Mongoo->Collection->update(array("capacidad" => $oldCapacidad), 	array('$set' => array("capacidad" => $newCapacidad))); 



		return $response->withRedirect("/intranet/bus/");

	}


	
}
