<?php 

namespace App\Controllers;


use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongoo;
 


class ObjetosPerdidosController extends Controller
{

	function ObjetosPerdidos($request, $response, $arg=[]){	
		//En esta función mostrará los Objetos Perdidos que hay en el grupo Objetos_Perdidos.
		$this->Mongoo->Swish("Objetos_Perdidos");
		$out['objetos_perdidos'] = $this->Mongoo->read($this->Mongoo->Collection->find());

		$this->Mongoo->Swish("Routes");
		$out['routes'] = $this->Mongoo->read($this->Mongoo->Collection->find());

		return $this->view->render($response, 'objetosPerdidos/objetosPerdidos.twig',  $out);

	}


	function add_objeto_perdido($request, $response, $arg=[]){
		//Entramos en el grupo Objetos_Perdidos. Esta función nos ayudará principalmente a implementar nuevos Objetos Perdidos.
		$this->Mongoo->Swish("Objetos_Perdidos");

		//carpeta donde se va a subir el archivo       
        $uploaddir = '/home/admin/web/buscontrol.hellomovertis.com/public_html/app/uploads/';
        //directorio + nombre del fichero
        $uploadfile = $uploaddir . basename($_FILES["filename"]["name"]);

        //mueve el archivo al directorio dicho        
        move_uploaded_file($_FILES["filename"]["tmp_name"], $uploadfile); 

        $_POST['foto'] = basename($_FILES["filename"]["name"]);
        
		$type = pathinfo($uploadfile, PATHINFO_EXTENSION);

		$data = file_get_contents($uploadfile);

		$fotoUri = 'data:image/' . $type . ';base64,' . base64_encode($data);

		$_POST['fotoURI'] = $fotoUri;

        $this->Mongoo->Collection->insert($_POST);

		return $response->withRedirect("/intranet/objetosPerdidos/");	

	}


	function delete_objeto_perdido($request, $response, $arg=[]){
		//Se entra en el grupo Objetos_Perdidos y se elimina lo seleccionado.
		$this->Mongoo->Swish("Objetos_Perdidos"); 
		
		$this->Mongoo->Collection->remove(array("nombre"=>$arg['nombre'])); 
		
		return $response->withRedirect("/intranet/objetosPerdidos/");
	}

	function mod_objeto_perdido($request, $response, $arg=[]){
		//Entramos en el grupo Objetos_Perdidos, donde inicializaremos las variables que nos ayudarán a modificar los datos dentro de Objetos_Perdidos.
		$this->Mongoo->Swish("Objetos_Perdidos");

		$oldName=			$_POST['old_name'];	
		$newName=			$_POST['new_name'];	
		$oldDescription=	$_POST['old_description'];	
		$newDescription=	$_POST['new_description'];
		$oldFoto=			$_POST['old_foto'];	
		$newFoto=			$_POST['new_foto'];
		
	

		$this->Mongoo->Collection->update(array("nombre" => $oldName), 		 		array('$set' => array("nombre" => $newName))); 
		$this->Mongoo->Collection->update(array("descripcion" => $oldDescription),  array('$set' => array("descripcion" => $newDescription))); 
		$this->Mongoo->Collection->update(array("foto" => $oldFoto), 				array('$set' => array("foto" => $newFoto))); 




		//Una vez hecho, se volverá a la página /intranet/objetosPerdidos/.
		return $response->withRedirect("/intranet/objetosPerdidos/");

	}


	
}
