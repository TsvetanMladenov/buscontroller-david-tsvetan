<?php 

namespace App\Controllers;


use App\Controllers\Controller;
use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use App\Utiles\Mongoo;
 


class ListaController extends Controller
{

	function lista($request, $response, $arg=[]){	
		
		//Entramos en los grupos Lista y Routes, leyendo lo que hay lo que hay dentro de ella
		$this->Mongoo->Swish("Lista");
		$out['lista'] = $this->Mongoo->read($this->Mongoo->Collection->find());

		$this->Mongoo->Swish("Routes");
		$out['routes'] = $this->Mongoo->read($this->Mongoo->Collection->find());

		return $this->view->render($response, 'lista/lista.twig',  $out);

	}

	function add_user($request, $response, $arg=[]){
		
		//Entramos en el grupo lista e insertamos los datos
		$this->Mongoo->Swish("Lista");
		$this->Mongoo->Collection->insert($_POST);

		return $response->withRedirect("/intranet/lista/");

	}
	function delete_user($request, $response, $arg=[]){

		//Entramos a Lista y Personas, y borramos con el remove.
		
		$this->Mongoo->Swish("Lista"); 

		$this->Mongoo->Collection->remove(array("name"=>$arg['name'])); 

		$this->Mongoo->Swish("Personas"); 

		$this->Mongoo->Collection->remove(array("lista_name"=>$arg['name']));
		
		return $response->withRedirect("/intranet/lista/");
	}

	function mod_lista($request, $response, $arg=[]){

		//Inicializamos las variables que iremos insertando en método POST
		
		$oldName=		$_POST['old_name'];	
		$newName=		$_POST['new_name'];	
		$oldRutaName=	$_POST['old_ruta_name'];	
		$newRutaName=	$_POST['new_ruta_name'];
		
		//Una vez inicializadas, las insertaremos en la base de datos y en los grupos Lista y Personas.
		$this->Mongoo->Swish("Lista");

		$this->Mongoo->Collection->update(array("name" => $oldName), 		 array('$set' => array("name" => $newName,"route_name" => $newRutaName))); 
		
		$this->Mongoo->Swish("Personas");

		$this->Mongoo->Collection->update(array("lista_name" => $oldName), 		 array('$set' => array("lista_name" => $newName)));

		
		return $response->withRedirect("/intranet/lista/");

	}
	function modifylista($request, $response, $arg=[]){

		$name=$arg['name'];

		//Para modificar el usuario, entramos en Lista o Grupo, buscamos el nombre, y lo modificaríamos, siempre y cuando se encuentre en la base de datos.
		$this->Mongoo->Swish("Lista");
		$lista = $this->Mongoo->Collection->findOne(array("name"=>$name),array('_id'=>0));
		$out['lista']=$lista;

		$this->Mongoo->Swish("Personas");
		$personas = $this->Mongoo->read($this->Mongoo->Collection->find(array("lista_name"=>$name)));
		$out['personas']= $personas;
		// echo "<pre>";
		// print_r ($personas);
		// die;
		return $this->view->render($response, 'lista/listamodify.twig',  $out);

	}


	
}
