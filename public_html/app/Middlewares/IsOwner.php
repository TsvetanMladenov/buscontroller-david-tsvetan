<?php

namespace App\Middlewares;

use App\Utiles\Mongoo as Mongoo;
 

class IsOwner
{
	private $prop = null;

	public function __construct($val) {
       
        $this->prop = $val;
    }

	public function __invoke($request, $response, $next){
		
		$Mongoo = new Mongoo($this->prop[0]);
		
		$r = array_values($request->getAttribute('routeInfo')[2]);

		$ar = array("".$this->prop[1].""=>$r[0], "owner"=>$_SESSION['owner']);
	 		
	 	$result = $Mongoo->Collection->findOne($ar);
	 		 
	 	if(!$result){


    		return $response->withStatus(401)
                             ->withHeader('Content-Type', 'application/json')
                             ->withJson(array('error','User is not Owner'));

	 	}else{
				
			$response = $next($request, $response);

			return $response;

	 	}

	}

}