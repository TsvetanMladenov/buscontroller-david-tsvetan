<?php

namespace App\Middlewares;

use App\Utiles\Mongoo;
use Firebase\JWT\JWT;

class AuthREST
{
    private static $secret_key = 'AIzaSyCB5UP4nfRGuf**bQje1FE2o65n_vV82asQY@';
    private static $encrypt = ['HS256'];
    private static $aud = null;
    

    public function __invoke($request, $response, $next){
    	
    	$headers = $request->getHeaders();
       
    	$token 	 = $_POST['token'];
 

    	if(self::Check($token)){

    		return $response->withStatus(401)
                             ->withHeader('Content-Type', 'application/json')
                             ->withJson(array('error','Invalid Token Check'));
    	}

    	$token_request = self::GetData($token);

        $response->user_properties['token_request'] = $token_request;

    	$response = $next($request, $response);

		return $response;
    }

    
    public static function Check($token)
    {
  
        if(empty($token))
        { 	
            return true;
        }
     
        try {

	        $decode = JWT::decode(
	            $token,
	            self::$secret_key,
	            self::$encrypt
	        );  

  
      
	        if($decode->aud !== self::Aud())
	        {

	          //   return true;
	        }  	
           
        } catch (Exception $e) {


        	 
        }

     
    }
    
    public static function GetData($token)
    {
          return JWT::decode(
                    $token,
                    self::$secret_key,
                    self::$encrypt )->data; 
    }
    
    private static function Aud()
    {
        $aud = '';
        
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $aud = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $aud = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $aud = $_SERVER['REMOTE_ADDR'];
        }
        
        $aud .= @$_SERVER['HTTP_USER_AGENT'];
        $aud .= gethostname();
        
        return sha1($aud);
    }
}

?>