﻿<?php 

 
    session_start();

    set_time_limit (0);

    define('ROOT_PATH'  , __DIR__.'/../');
    define('VENDOR_PATH', __DIR__.'/../vendor/');
    define('APP_PATH'   , __DIR__.'/');
    define('PUBLIC_PATH', __DIR__.'/../public/');
    /* */
    require VENDOR_PATH. 'autoload.php';

    include(VENDOR_PATH."/firebase/php-jwt/src/JWT.php");

    // InDevelop();
    // error_reporting(E_ALL);
    // ini_set("display_errors", 1);
    /** 
        * Añadimos tantas variables como queramos a la app 
        * Entre ellas las conexiones de mongo
    */
    $config = array(
        'path.root'     => ROOT_PATH,
        'path.public'   => PUBLIC_PATH,
        'path.app'      => APP_PATH
    );
    /**
     * incluimos librerias de Configuracion
     */
    foreach (glob(APP_PATH.'Config/*.php') as $configFile) {
        require $configFile;
    }
    /**
     * incluimos librerias de Middlewares
     */
    // foreach (glob(APP_PATH.'Middlewares/*.php') as $Middlewares) {
    //     require $Middlewares;
    // }



    $app = new \Slim\App($config);

 
   
    $container = $app->getContainer();

 
    /**
     * Abrimos conexion a twig desde el $container view
     * @author Gianni <[<email address>]>
     */
    $container['view'] = function ($c) {

        $view = new \Slim\Views\Twig(APP_PATH.'Views', [
            'cache' => false
        ]);
        
        $view->addExtension(new Slim\Views\TwigExtension(
            $c->router,
            $c->request->getUri()
        ));


        return $view;
    };

    /**
     * Abrimos conexion a mongo desde el $container Mongoo
     * Con la tabla por defecto en hello_users
     * @author Gianni <[<email address>]>
     */
    $container['Mongoo'] = function ($c) {

        return new \App\Utiles\Mongoo("Users");
    
    };
    /**
     * Abrimos conexion a memcache desde el $container Memcache
     * @author Gianni <[<email address>]>
     */
    $container['Memcache'] = function ($c) {

        return new \App\Utiles\Memcache();
    
    };

    $container['Fire'] = function ($c) {

        return new \PhpFirebase\Firebase('https://assistantmovertis-6a53e.firebaseio.com','kPdpH96hmDt90wUHtZHBpTq3V0u8MtPmaaP8VfSR');
    
    };

    /**
     * Abrimos conexion a memcache desde el $container Memcache
     * @author Gianni <[<email address>]>
     */
    $container['Crypto'] = function ($c) {

        return new \App\Utiles\Crypto();
    
    };

 

    $container['path'] = function ($c) {
        return $_SERVER['DOCUMENT_ROOT'];
    };



    $_SESSION['url'] = '//'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];


    function InDevelop(){

        if($_SESSION['login_user']['username']=="gianni" ){
              
            error_reporting(E_ALL);
            ini_set("display_errors", 1);

        }else{
            include(APP_PATH."/Views/trabajando.tpl.twig");
            die;
        }
    }
 
    $container['mailer'] = function ($container) {
      
        $mailer = new PHPMailer();
        
        //$mailer->SMTPDebug = 3;
        $mailer->IsSMTP();

        $mailer->Host = 'ssl://smtp.gmail.com';  
        $mailer->SMTPAuth = true;                 // I set false for localhost
        $mailer->SMTPSecure = 'ssl';              // set blank for localhost
        $mailer->Port = 465;                           // 25 for local host
        $mailer->Username = 'hello@movertis.com';    // I set sender email in my mailer call
        $mailer->Password = 'neutrino55';
        $mailer->isHTML(true);

        return new \App\Mail\Mailer($container->view, $mailer);
    };


    require APP_PATH. 'routes.php';

